package net.quadforge.flyingfishgroundcontrol;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class SettingsActivity extends AppCompatActivity implements Button.OnClickListener{

    private final String TAG = "FlyingFishGCS";

    ArrayAdapter<CharSequence> adapter;

    EditText altSetting, radiusSetting;
    Button increment_alt, decrement_alt;
    Button increment_rad, decrement_rad;
    Button save;
    Spinner mapProviderSpinner;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        altSetting = (EditText) findViewById(R.id.alt_setting);
        radiusSetting = (EditText) findViewById(R.id.radius_setting);
        increment_alt = (Button) findViewById(R.id.intcrement_alt_button);
        decrement_alt = (Button) findViewById(R.id.decrement_alt_button);
        increment_rad = (Button) findViewById(R.id.increment_radius_button);
        decrement_rad = (Button) findViewById(R.id.decrement_radius_button);
        save = (Button) findViewById(R.id.save_settings_button);
        mapProviderSpinner = (Spinner) findViewById(R.id.map_source_spinner);

        increment_alt.setOnClickListener(this);
        decrement_alt.setOnClickListener(this);
        increment_rad.setOnClickListener(this);
        decrement_rad.setOnClickListener(this);
        save.setOnClickListener(this);

        preferences = getSharedPreferences(getString(R.string.pref_file) ,Context.MODE_PRIVATE);

        adapter = ArrayAdapter.createFromResource(this, R.array.map_providers, R.layout.support_simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        mapProviderSpinner.setAdapter(adapter);

        loadDefualtsFromPreferences();

    }

    @Override
    protected void onResume(){
        super.onResume();
        loadDefualtsFromPreferences();
    }

    private void loadDefualtsFromPreferences(){
        altSetting.setText(preferences.getString(getString(R.string.pref_alt), getString(R.string.pref_default_alt)));
        radiusSetting.setText(preferences.getString(getString(R.string.pref_default_rad), getString(R.string.pref_default_rad)));
        mapProviderSpinner.setSelection(preferences.getInt(getString(R.string.map_provider), 0));
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.intcrement_alt_button:
                altSetting.setText(String.valueOf(Double.valueOf(altSetting.getText().toString()) + 1));
                break;
            case R.id.increment_radius_button:
                radiusSetting.setText(String.valueOf(Double.valueOf(radiusSetting.getText().toString()) + 1));
                break;
            case R.id.decrement_alt_button:
                altSetting.setText(String.valueOf(Double.valueOf(altSetting.getText().toString()) - 1));
                break;
            case R.id.decrement_radius_button:
                radiusSetting.setText(String.valueOf(Double.valueOf(radiusSetting.getText().toString()) - 1));
                break;
            case R.id.save_settings_button:
                Log.i(TAG, "save pressed");
                editor = preferences.edit();
                editor.putString(getString(R.string.pref_alt), altSetting.getText().toString());
                editor.putString(getString(R.string.pref_default_rad), radiusSetting.getText().toString());
                editor.putInt(getString(R.string.map_provider), mapProviderSpinner.getSelectedItemPosition());
                editor.apply();
                Snackbar.make(v, "Settings Saved", Snackbar.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
    }
}
