package net.quadforge.flyingfishgroundcontrol;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.o3dr.android.client.ControlTower;
import com.o3dr.android.client.Drone;
import com.o3dr.android.client.apis.MissionApi;
import com.o3dr.android.client.apis.VehicleApi;
import com.o3dr.android.client.interfaces.DroneListener;
import com.o3dr.android.client.interfaces.TowerListener;
import com.o3dr.services.android.lib.drone.attribute.AttributeEvent;
import com.o3dr.services.android.lib.drone.attribute.AttributeType;
import com.o3dr.services.android.lib.drone.connection.ConnectionParameter;
import com.o3dr.services.android.lib.drone.connection.ConnectionResult;
import com.o3dr.services.android.lib.drone.connection.ConnectionType;
import com.o3dr.services.android.lib.drone.mission.Mission;
import com.o3dr.services.android.lib.drone.property.Altitude;
import com.o3dr.services.android.lib.drone.property.Battery;
import com.o3dr.services.android.lib.drone.property.Gps;
import com.o3dr.services.android.lib.drone.property.Speed;
import com.o3dr.services.android.lib.drone.property.Type;
import com.o3dr.services.android.lib.drone.property.VehicleMode;

public class MainActivity extends AppCompatActivity implements FlightDataFragment.OnFlightDataFragmentInteractionListener,
                                                               MapFragment.OnMapFragmentInteractionListener,
                                                               EditWaypointFragment.OnEditFragmentInteractionListener,
                                                               TowerListener,
                                                               DroneListener{

    private final String TAG = "FlyingFishGCS/Main";

    FragmentManager fragmentManager = getSupportFragmentManager();
    Bundle extraParams;
    Handler handler;

    private MapFragment mapFragment;
    private FlightDataFragment flightDataFragment;

    private Drone drone;
    private ControlTower controlTower;
    private int droneType = Type.TYPE_UNKNOWN;

    private void swapToEditMode(){
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.flight_data_container, flightDataFragment);
        transaction.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mapFragment = MapFragment.newInstance(null, null);
        flightDataFragment = FlightDataFragment.newInstance(null, null);

        mapFragment.setArguments(getIntent().getExtras());
        flightDataFragment.setArguments(getIntent().getExtras());

        fragmentManager.beginTransaction().add(R.id.map_container, mapFragment).commit();
        fragmentManager.beginTransaction().add(R.id.flight_data_container, flightDataFragment).commit();

        controlTower = new ControlTower(this);
        controlTower.connect(this);
        handler = new Handler();
        drone = new Drone(this);
    }

    public int getDroneType(){
        return droneType;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch(id){
            case R.id.action_settings:
                Log.i(TAG, "Starting settings activity");
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.connect:
                Log.i(TAG, "Connect was selected");
                extraParams = new Bundle();
                extraParams.putInt(ConnectionType.EXTRA_USB_BAUD_RATE, 57600);
                ConnectionParameter connectionParameter = new ConnectionParameter(ConnectionType.TYPE_USB, extraParams, null);
                if(item.getTitle().toString().equals("Connect")){
                    VehicleApi.getApi(drone).connect(connectionParameter);
                    item.setTitle("Disconnect");
                }
                else{
                    VehicleApi.getApi(drone).disconnect();
                    item.setTitle("Connect");
                }
                return true;
            case R.id.send_mission:
                Log.i(TAG, "Send mission selected");
                MissionApi.getApi(drone).setMission(mapFragment.makeMission(), true);
                return true;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    //Interacting with the drone

    @Override
    public void onTowerConnected() {
        Snackbar.make(findViewById(R.id.map_container), "Connected to 3DR Services", Snackbar.LENGTH_SHORT);
        controlTower.registerDrone(drone, handler);
        drone.registerDroneListener(this);
    }

    @Override
    public void onTowerDisconnected() {
        Snackbar.make(findViewById(R.id.map_container), "Disconnected from 3DR Services", Snackbar.LENGTH_SHORT);
    }

    @Override
    public void editWaypoint(double lat, double lon, double alt, double rad, int index) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.flight_data_container, EditWaypointFragment.newInstance(lat, lon, alt, rad, index));
        transaction.commit();
    }

    @Override
    public void saveWaypoint(double alt, double rad, int index) {
        mapFragment.updateWaypoint(alt, rad, index);
        swapToEditMode();
    }

    @Override
    public void deleteWaypoint(int index) {
        mapFragment.deleteWaypoint(index);
        swapToEditMode();
    }

    @Override
    public void onDroneConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "Failed to connect to drone");
    }

    @Override
    public void onDroneEvent(String event, Bundle extras) {
        Gps gps;
        switch (event){
            case AttributeEvent.STATE_CONNECTED:
                Log.i(TAG, "Drone connected");
                Snackbar.make(findViewById(R.id.map_container), "Connected to drone", Snackbar.LENGTH_SHORT).show();
                flightDataFragment.updateConnectionState(true);
                break;
            case AttributeEvent.STATE_DISCONNECTED:
                Log.i(TAG, "Drone disconnected");
                Snackbar.make(findViewById(R.id.map_container), "Disconnected from drone", Snackbar.LENGTH_SHORT).show();
                flightDataFragment.updateConnectionState(false);
                break;
            case AttributeEvent.STATE_VEHICLE_MODE:
                break;
            case AttributeEvent.TYPE_UPDATED:
                droneType = ((Type)drone.getAttribute(AttributeType.TYPE)).getDroneType();
                break;
            case AttributeEvent.SPEED_UPDATED:
                flightDataFragment.updateSpeedValue(((Speed) drone.getAttribute(AttributeType.SPEED)).getGroundSpeed());
                break;
            case AttributeEvent.ALTITUDE_UPDATED:
                flightDataFragment.updateAltitudeValue(((Altitude) drone.getAttribute(AttributeType.ALTITUDE)).getAltitude());
                break;
            case AttributeEvent.BATTERY_UPDATED:
                flightDataFragment.updateBatteryValue(((Battery) drone.getAttribute(AttributeType.BATTERY)).getBatteryVoltage());
                break;
            case AttributeEvent.GPS_FIX:
                gps = drone.getAttribute(AttributeType.GPS);
                flightDataFragment.updateGpsValue(gps.getSatellitesCount());
                mapFragment.updateDroneLocation(gps.getPosition().getLatitude(), gps.getPosition().getLongitude());
                break;
            case AttributeEvent.GPS_POSITION:
                gps = drone.getAttribute(AttributeType.GPS);
                mapFragment.updateDroneLocation(gps.getPosition().getLatitude(), gps.getPosition().getLongitude());
                break;
            case AttributeEvent.MISSION_RECEIVED:
                Snackbar.make(findViewById(R.id.map_container), "Mission sent", Snackbar.LENGTH_SHORT);
            default:
                break;
        }
    }

    @Override
    public void onDroneServiceInterrupted(String errorMsg) {

    }

    @Override
    public void stabilizeButtonClick() {
        VehicleApi.getApi(drone).setVehicleMode(VehicleMode.COPTER_STABILIZE);
    }

    @Override
    public void rtlButtonClick() {
        switch(droneType){
            case Type.TYPE_COPTER:
                VehicleApi.getApi(drone).setVehicleMode(VehicleMode.COPTER_RTL);
                break;
            case Type.TYPE_ROVER:
                VehicleApi.getApi(drone).setVehicleMode(VehicleMode.ROVER_RTL);
        }
    }

    @Override
    public void armButonClick(boolean armDisarm) {
        // true -> arm
        // false -> disarm
        VehicleApi.getApi(drone).arm(armDisarm);
    }

    @Override
    public void launchButtonClick() {
        MissionApi.getApi(drone).setMission(mapFragment.makeMission(), true);
        MissionApi.getApi(drone).startMission(true, false, null);
    }
}
