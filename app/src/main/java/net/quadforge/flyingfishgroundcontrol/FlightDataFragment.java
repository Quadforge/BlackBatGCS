package net.quadforge.flyingfishgroundcontrol;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.api.defaults.DefaultBootstrapBrand;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFlightDataFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FlightDataFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FlightDataFragment extends Fragment implements BootstrapButton.OnClickListener{

    ViewGroup controlButtons;
    TextView speedValue, altitudeValue, batteryValue, gpsValue;

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFlightDataFragmentInteractionListener mListener;

    public FlightDataFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FlightDataFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FlightDataFragment newInstance(String param1, String param2) {
        FlightDataFragment fragment = new FlightDataFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_flight_data, container, false);
        controlButtons = (ViewGroup) view.findViewById(R.id.flight_control_buttons);

        speedValue = (TextView) view.findViewById(R.id.speed_value);
        altitudeValue = (TextView) view.findViewById(R.id.altitude_value);
        batteryValue = (TextView) view.findViewById(R.id.battery_value);
        gpsValue = (TextView) view.findViewById(R.id.gps_value);

        for(int i = 0; i < controlButtons.getChildCount(); i++){
            controlButtons.getChildAt(i).setOnClickListener(this);
        }

        return view;
    }

    protected void updateSpeedValue(double speed){
        speedValue.setText(String.format("%3.1f",speed) + "m/s");
    }

    protected void updateAltitudeValue(double altitude){
        altitudeValue.setText(String.format("%3.1f", altitude) + "m");
    }

    protected void updateBatteryValue(double battery){
        batteryValue.setText(String.format("%3.1f", battery) + "v");
    }

    protected void updateGpsValue(int satCount){
        gpsValue.setText(String.valueOf(satCount));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFlightDataFragmentInteractionListener) {
            mListener = (OnFlightDataFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnMapFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void updateConnectionState(boolean isConnected){
        for(int i = 0; i < controlButtons.getChildCount(); i++){
            if(isConnected){
                ((BootstrapButton) controlButtons.getChildAt(i)).setBootstrapBrand(DefaultBootstrapBrand.SUCCESS);
            }
            else{
                ((BootstrapButton) controlButtons.getChildAt(i)).setBootstrapBrand(DefaultBootstrapBrand.SECONDARY);

            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.stabilize_button:
                mListener.stabilizeButtonClick();
                break;
            case R.id.rtl_button:
                mListener.rtlButtonClick();
                break;
            case R.id.arm_button:
                if(((BootstrapButton) v).getText().toString().equals("arm")){
                    mListener.armButonClick(true);
                    ((BootstrapButton) v).setText("diarm");
                }
                else{
                    ((BootstrapButton) v).setText("arm");
                    mListener.armButonClick(false);
                }
                break;
            case R.id.launch_button:
                mListener.launchButtonClick();
            default:
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFlightDataFragmentInteractionListener {
        // TODO: Update argument type and name
        void stabilizeButtonClick();
        void rtlButtonClick();
        void armButonClick(boolean armDisarm);
        void launchButtonClick();
    }
}
