package net.quadforge.flyingfishgroundcontrol;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.o3dr.services.android.lib.coordinate.LatLongAlt;
import com.o3dr.services.android.lib.drone.mission.Mission;
import com.o3dr.services.android.lib.drone.mission.item.command.Takeoff;
import com.o3dr.services.android.lib.drone.mission.item.spatial.Land;
import com.o3dr.services.android.lib.drone.mission.item.spatial.Waypoint;
import com.o3dr.services.android.lib.drone.property.Type;

import org.osmdroid.events.MapEventsReceiver;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.tileprovider.tilesource.bing.BingMapTileSource;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.MapEventsOverlay;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.gestures.RotationGestureOverlay;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnMapFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MapFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MapFragment extends Fragment implements MapEventsReceiver, Marker.OnMarkerClickListener, View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private final int BING_MAP = 0;
    private final int USGS = 1;
    private final int OPEN_STEET_MAP = 2;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private final String TAG = "FlyingFishGCS/Map";

    private OnMapFragmentInteractionListener mListener;

    private SharedPreferences preferences;

    private TextView canEditMissionLabel;
    private FloatingActionButton fab;

    private MapView map;
    private MyLocationNewOverlay myLocationNewOverlay;
    private MapEventsOverlay mapEventsOverlay;
    private MapController mapController;
    private RotationGestureOverlay rotationGestureOverlay;

    private Marker droneMarker = null;

    private ArrayList<Marker> markers = new ArrayList<>();
    private ArrayList<Waypoint> waypoints = new ArrayList<>();

    private boolean canEditMission = false;


    public MapFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MapFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MapFragment newInstance(String param1, String param2) {
        MapFragment fragment = new MapFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public void setUpMap(int mapProvider){
        switch(mapProvider){
            case BING_MAP:
                setUpBingMap();
                break;
            case OPEN_STEET_MAP:
                setUpOSM();
                break;
            case USGS:
                setUpUSGS();
                break;
            default:
                break;
        }

        mapController = new MapController(map);
        myLocationNewOverlay = new MyLocationNewOverlay(new GpsMyLocationProvider(getContext()), map);
        myLocationNewOverlay.runOnFirstFix(new Runnable() {
            @Override
            public void run(){
                Log.i(TAG, "Found user location");
                mapController.animateTo(myLocationNewOverlay.getMyLocation());
                mapController.setCenter(myLocationNewOverlay.getMyLocation());
            }
        });
        mapEventsOverlay = new MapEventsOverlay(getContext(), this);
        map.getOverlays().add(mapEventsOverlay);
        rotationGestureOverlay = new RotationGestureOverlay(getContext(), map);
        rotationGestureOverlay.setEnabled(true);
        map.setMultiTouchControls(true);
        map.getOverlays().add(rotationGestureOverlay);
        myLocationNewOverlay.enableMyLocation();
        map.getOverlays().add(myLocationNewOverlay);
        mapController.setZoom(15);
        map.setFlingEnabled(false);
    }

    public void setUpBingMap(){
        org.osmdroid.tileprovider.tilesource.bing.BingMapTileSource.retrieveBingKey(getContext());
        org.osmdroid.tileprovider.tilesource.bing.BingMapTileSource bing = new org.osmdroid.tileprovider.tilesource.bing.BingMapTileSource(BingMapTileSource.IMAGERYSET_AERIAL);
        bing.setStyle(BingMapTileSource.IMAGERYSET_AERIAL);
        map.setTileSource(bing);
    }

    public void setUpOSM(){
        map.setTileSource(TileSourceFactory.MAPNIK);
    }


    public void setUpUSGS(){
        map.setTileSource(TileSourceFactory.USGS_SAT);
    }

    public void setCanEditMission(boolean canEditMission){
        this.canEditMission = canEditMission;
    }

    public boolean getCanEditMission(){
        return canEditMission;
    }

    public void updateWaypoint(double alt, double rad, int index){
        Waypoint waypoint = waypoints.get(index);
        waypoint.setCoordinate(new LatLongAlt(waypoint.getCoordinate().getLatitude(), waypoint.getCoordinate().getLongitude(), alt));
        waypoint.setAcceptanceRadius(rad);
        waypoints.set(index, waypoint);
    }

    public void deleteWaypoint(int index){
        waypoints.remove(index);
        waypoints.trimToSize();
        markers.get(index).remove(map);
        map.invalidate();
        markers.remove(index);
        markers.trimToSize();
        for(int i = 0; i < markers.size(); i++){
            markers.get(i).setTitle(String.valueOf(i + 1));
        }
    }

    protected void updateDroneLocation(double lat, double lon){
        if(droneMarker == null){
            droneMarker = new Marker(map);
            if(((MainActivity) getActivity()).getDroneType() == Type.TYPE_ROVER){
                droneMarker.setIcon(getResources().getDrawable(R.drawable.ic_rover));
            }
            else{
                droneMarker.setIcon(getResources().getDrawable(R.drawable.ic_local_airport_white_18dp));
            }
            droneMarker.setPosition(new GeoPoint(lat, lon));
            map.getOverlays().add(droneMarker);
            map.invalidate();
        }
    }

    private Waypoint generateWaypointFromMarker(Marker marker){
        Waypoint waypoint = new Waypoint();
        GeoPoint p = marker.getPosition();
        waypoint.setCoordinate(new LatLongAlt(p.getLatitude(), p.getLongitude(), Double.valueOf(preferences.getString(getResources().getString(R.string.pref_alt), getResources().getString(R.string.pref_default_alt)))));
        waypoint.setAcceptanceRadius(Double.valueOf(preferences.getString(getResources().getString(R.string.pref_rad), getResources().getString(R.string.pref_default_rad))));
        return waypoint;
    }

    private Waypoint getWaypointFromMarker(Marker marker){
        int i = Integer.valueOf(marker.getTitle()) - 1;
        return waypoints.get(Integer.valueOf(marker.getTitle()) - 1);
    }

    private int getMarkerIndex(Marker marker){
        return Integer.valueOf(marker.getTitle()) - 1;
    }

    protected Mission makeMission(){
        Mission mission = new Mission();
        Land land = new Land();
        mission.addMissionItem(new Takeoff());
        for(Waypoint waypoint : waypoints){
            mission.addMissionItem(waypoint);
        }
        land.setCoordinate(((Waypoint)mission.getMissionItem(mission.getMissionItems().size() - 1)).getCoordinate());
        return mission;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        preferences = getActivity().getSharedPreferences(getString(R.string.pref_file), Context.MODE_PRIVATE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        map = (MapView) view.findViewById(R.id.map);

        canEditMissionLabel = (TextView) view.findViewById(R.id.edit_mode_state);
        fab = (FloatingActionButton) view.findViewById(R.id.fab);

        fab.setOnClickListener(this);
        return view;
    }

    @Override
    public void onResume(){
        super.onResume();
        setUpMap(preferences.getInt(getString(R.string.map_provider), 0));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMapFragmentInteractionListener) {
            mListener = (OnMapFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnMapFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public boolean singleTapConfirmedHelper(GeoPoint p) {
        if(canEditMission) {
            Marker marker = new Marker(map);
            marker.setPosition(p);
            markers.add(marker);
            marker.setTitle(String.valueOf(markers.size()));
            waypoints.add(generateWaypointFromMarker(marker));
            marker.setOnMarkerClickListener(this);
            map.getOverlays().add(marker);
            map.invalidate();
        }
        return false;
    }

    @Override
    public boolean longPressHelper(GeoPoint p) {
        return false;
    }

    @Override
    public boolean onMarkerClick(Marker marker, MapView mapView) {
        Log.i(TAG, "marker tapped");
        Waypoint waypoint = getWaypointFromMarker(marker);
        LatLongAlt coordinate = waypoint.getCoordinate();
        mListener.editWaypoint(coordinate.getLatitude(), coordinate.getLongitude(), coordinate.getAltitude(), waypoint.getAcceptanceRadius(), getMarkerIndex(marker));
        return false;
    }


    //Jacob Tarlo
    @Override
    public void onClick(View v) {
        canEditMission = !canEditMission;
        String newLabel;
        int newLabelColor;
        newLabelColor = canEditMission ? getResources().getColor(R.color.bootstrap_brand_success) : getResources().getColor(R.color.bootstrap_brand_danger);
        newLabel = canEditMission ? "Enabled" : "Disabled";
        canEditMissionLabel.setText(newLabel);
        canEditMissionLabel.setTextColor(newLabelColor);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnMapFragmentInteractionListener {
        // TODO: Update argument type and name
        void editWaypoint(double lat, double lon, double alt, double rad, int index);
    }
}
