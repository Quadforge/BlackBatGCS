package net.quadforge.flyingfishgroundcontrol;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapButton;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnEditFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EditWaypointFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditWaypointFragment extends Fragment implements Button.OnClickListener{

    //UI elements
    private EditText waypointAltField, waypointRadField;
    private TextView waypointNumber;
    private BootstrapButton incrementAlt, decrementAlt, incrementRad, decrementRad, saveButton, deleteButton;

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String LAT = "lat";
    private static final String LON = "lon";
    private static final String ALT = "alt";
    private static final String RAD = "rad";
    private static final String INDEX = "index";

    //parameters
    private double lat, lon, alt, rad;
    private int markerIndex;

    private OnEditFragmentInteractionListener mListener;

    public EditWaypointFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param lat latitude.
     * @param lon longitude.
     * @param alt altitude.
     * @param rad acceptance radius.
     * @return A new instance of fragment EditWaypointFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EditWaypointFragment newInstance(Double lat, Double lon, Double alt, Double rad, int markerIndex) {
        EditWaypointFragment fragment = new EditWaypointFragment();
        Bundle args = new Bundle();
        args.putDouble(LAT, lat);
        args.putDouble(LON, lon);
        args.putDouble(ALT, alt);
        args.putDouble(RAD, rad);
        args.putInt(INDEX, markerIndex);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            lat = getArguments().getDouble(LAT);
            lon = getArguments().getDouble(LON);
            alt = getArguments().getDouble(ALT);
            rad = getArguments().getDouble(RAD);
            markerIndex = getArguments().getInt(INDEX);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_waypoint, container, false);

        waypointNumber = (TextView) view.findViewById(R.id.waypoint_number);

        waypointAltField = (EditText) view.findViewById(R.id.waypoint_alt_field);
        incrementAlt = (BootstrapButton) view.findViewById(R.id.increment_waypoint_alt_button);
        decrementAlt = (BootstrapButton) view.findViewById(R.id.decrement_waypoint_alt_button);

        waypointRadField = (EditText) view.findViewById(R.id.waypoint_rad_field);
        incrementRad = (BootstrapButton) view.findViewById(R.id.increment_waypoint_rad_button);
        decrementRad = (BootstrapButton) view.findViewById(R.id.decrement_waypoint_rad_button);

        saveButton = (BootstrapButton) view.findViewById(R.id.save_waypoint);
        deleteButton = (BootstrapButton) view.findViewById(R.id.delete_waypoint);

        incrementAlt.setOnClickListener(this);
        decrementAlt.setOnClickListener(this);
        incrementRad.setOnClickListener(this);
        decrementRad.setOnClickListener(this);

        saveButton.setOnClickListener(this);
        deleteButton.setOnClickListener(this);

        waypointAltField.setText(String.valueOf(alt));
        waypointRadField.setText(String.valueOf(rad));
        waypointNumber.setText(String.valueOf(markerIndex + 1));

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnEditFragmentInteractionListener) {
            mListener = (OnEditFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnMapFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        double currentAlt = Double.valueOf(waypointAltField.getText().toString());
        double currentRad = Double.valueOf(waypointRadField.getText().toString());
        switch (view.getId()){
            case R.id.increment_waypoint_alt_button:
                waypointAltField.setText(String.valueOf(currentAlt + 1));
                break;
            case R.id.decrement_waypoint_alt_button:
                waypointAltField.setText(String.valueOf(currentAlt - 1));
                break;
            case R.id.increment_waypoint_rad_button:
                waypointRadField.setText(String.valueOf(currentRad + 1));
                break;
            case R.id.decrement_waypoint_rad_button:
                waypointRadField.setText(String.valueOf(currentRad - 1));
                break;
            case R.id.save_waypoint:
                mListener.saveWaypoint(currentAlt, currentRad, markerIndex);
                break;
            case R.id.delete_waypoint:
                mListener.deleteWaypoint(markerIndex);
                break;
            default:
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnEditFragmentInteractionListener {
        void saveWaypoint(double alt, double rad, int index);
        void deleteWaypoint(int index);
    }
}
